# Write your code here to make the tests pass.
#
# Change your working directory to this directory,
# linked_list.
#
# Start by running python -m pytest tests/test_01.py and
# making the test pass.
#
# Then, run python -m pytest tests/test_02.py to make the
# next test pass. Keep going to tests/test_19.py.

class LinkedListNode:
    def __init__(self, value, link=None):
        self.value = value
        self.link = link

class LinkedList:
    def __init__(self, head=None, tail=None):
        self.head = head
        self.tail = tail
        self.length = 0

    def length(self):
        return self.length

    def insert(self, value, index=None):

        node = LinkedListNode(value)

        if self.head == None:
            self.head = node
            self.tail = node
        else:
            self.tail.link = node
            self.tail = node

        self.length += 1

    def get(self, index):
        pass
